#ifndef COMMANDLINEARGS_H
#define COMMANDLINEARGS_H

#include <iostream>
using namespace std;

class CommandLineArgs {
  public:
  void helpText();
  void paramParse(int ac, char* av[]);
};

#endif