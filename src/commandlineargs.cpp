#include "commandlineargs.hpp"
#include "filehandler.hpp"


void CommandLineArgs::helpText(){
    cout << "-------------------------------------------------" << endl;
    cout << "lofian - the command line (lo)g (fi)le (an)alyser" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Usage: ./lofian [option(s)]" << endl;
    cout << "Options: " << endl;
    cout << "   -h : This text." << endl;
    cout << "   -f : Log file name." << endl;
    cout << "   -fl : Log file list in another file." << endl;
    cout << "   -r : Regex input." << endl;
    cout << "   -cr : File that contains custom regex" << endl;
}

void CommandLineArgs::paramParse(int ac, char* av[]){
    for (int i = 0; i < ac; i++){
      if (av[i][0] == '-') {
        switch(av[i][1]) {
          case 'h' :
            helpText();
            break;
          case 'f':
            FileHandler fh;
            fh.handleFile(av[i+1], av[i+3]);
            break;
        }
      }
    }
}