#ifndef FILEHANDLER_H
#define FILEHANDLER_H

#include <iostream>
#include <fstream>
#include <string>
#include <regex>
using namespace std;

class FileHandler {
  public:
  void handleFile(string filename, string regex_str);
};

#endif