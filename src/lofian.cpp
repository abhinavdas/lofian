#include "filehandler.hpp"
#include "commandlineargs.hpp"
using namespace std;

int main(int argc, char* argv[]){
  CommandLineArgs cla;
  cla.paramParse(argc, argv);
  return 0;
}