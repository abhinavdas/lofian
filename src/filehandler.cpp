#include "filehandler.hpp"

void FileHandler::handleFile(string filename, string regex_str){
  if (ifstream(filename)){
    string s;
    int lineno = 0;
    regex reg1(regex_str);
    ifstream logfile(filename);
    while(!logfile.eof()){
      ++lineno;
      getline(logfile, s);
      if (regex_search(s, reg1)){
        cout << "Pattern found at line: ";
        cout << lineno << endl;
      }
    }
  } else {
    cout << "File doesn't exist." << endl;
  }
}